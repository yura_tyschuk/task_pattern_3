package factory;

import flowers.implementation.Bouquet;
import flowers.type.BouquetType;

public abstract class FlowersShop {
  protected abstract Bouquet createBouquet(BouquetType bouquetType);

  public Bouquet prepare(BouquetType bouquetType) {
    Bouquet bouquet = createBouquet(bouquetType);
    bouquet.prepare();
    bouquet.sell();

    return bouquet;
  }
}
