package factory.implementation;

import factory.FlowersShop;
import flowers.implementation.Bouquet;
import flowers.implementation.PeoniesBouquet;
import flowers.implementation.RosesBouquet;
import flowers.implementation.TulipsBouquet;
import flowers.type.BouquetType;

public class KyivShop extends FlowersShop {

  @Override
  protected Bouquet createBouquet(BouquetType bouquetType) {
    Bouquet bouquet = null;
    if (bouquetType == BouquetType.ROSES) {
      bouquet = new RosesBouquet();
    } else if (bouquetType == BouquetType.TULIPS) {
      bouquet = new TulipsBouquet();
    } else if (bouquetType == BouquetType.PEONIES) {
      bouquet = new PeoniesBouquet();
    }
    return bouquet;
  }

}
