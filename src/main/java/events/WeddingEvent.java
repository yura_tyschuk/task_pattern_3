package events;

public class WeddingEvent implements Event {

  @Override
  public void show() {
    logger.warn("Flowers for wedding");
  }
}
