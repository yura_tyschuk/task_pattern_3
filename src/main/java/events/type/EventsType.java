package events.type;

public enum EventsType {
  WEDDING, BIRTHDAY, PARTY;
}
