package events;

public class BirthdayEvent implements Event {

  @Override
  public void show() {
    logger.warn("Flowers for birthday event");
  }
}
