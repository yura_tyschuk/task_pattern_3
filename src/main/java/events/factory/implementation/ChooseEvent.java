package events.factory.implementation;

import events.BirthdayEvent;
import events.Event;
import events.PartyEvent;
import events.WeddingEvent;
import events.type.EventsType;

public class ChooseEvent extends ForWhatBouquet {

  @Override
  protected Event chooseEvent(EventsType eventsType) {
    Event event = null;

    if (eventsType == EventsType.BIRTHDAY) {
      event = new BirthdayEvent();
    } else if (eventsType == EventsType.PARTY) {
      event = new PartyEvent();
    } else if (eventsType == EventsType.WEDDING) {
      event = new WeddingEvent();
    }
    return event;
  }
}
