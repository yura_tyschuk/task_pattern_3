package events.factory.implementation;

import events.Event;
import events.type.EventsType;

public abstract class ForWhatBouquet {

  protected abstract Event chooseEvent(EventsType eventsType);

  public Event prepare(EventsType eventsType) {
    Event event = chooseEvent(eventsType);
    event.show();

    return event;
  }
}
