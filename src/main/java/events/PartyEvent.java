package events;

public class PartyEvent implements Event {

  @Override
  public void show() {
    logger.warn("Flowers for party");
  }
}
