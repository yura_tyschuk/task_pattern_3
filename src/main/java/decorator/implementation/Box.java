package decorator.implementation;

import box.variations.BoxBuild;
import box.variations.BoxType;
import box.variations.implementations.BoxVariations;
import box.variations.implementations.TracingVariations;
import decorator.BouquetDecoratorClass;

public class Box extends BouquetDecoratorClass {

  BoxType boxType;
  BoxBuild boxBuild;


  public Box(BoxType boxType) {
    this.boxType = boxType;

    switch (boxType) {
      case BOX:
        boxBuild = new BoxBuild(new BoxVariations());
        setAdditionPrice(boxBuild.getPrice());
        setToName(boxBuild.getToName());
        break;
      case TRACING:
        boxBuild = new BoxBuild(new TracingVariations());
        setAdditionPrice(boxBuild.getPrice());
        setToName(boxBuild.getToName());
        break;
    }
  }
}
