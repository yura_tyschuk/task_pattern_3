package decorator.implementation;

import decorator.BouquetDecoratorClass;
import delivery.variations.DeliveryBuild;
import delivery.variations.DeliveryType;
import delivery.variations.implementations.FastDelivery;
import delivery.variations.implementations.FreeDelivery;
import delivery.variations.implementations.NormalDelivery;

public class Delivery extends BouquetDecoratorClass {

  DeliveryType deliveryType;
  DeliveryBuild deliveryBuild;
  public Delivery() {

  }
  public Delivery(DeliveryType deliveryType) {

    this.deliveryType = deliveryType;
    this.deliveryBuild = new DeliveryBuild(new FastDelivery());

    switch (deliveryType) {
      case FAST:
        deliveryBuild = new DeliveryBuild(new FastDelivery());
        setAdditionPrice(deliveryBuild.getPrice());
        setToName(deliveryBuild.getToName());
        break;
      case FREE:
        deliveryBuild = new DeliveryBuild(new FreeDelivery());
        setAdditionPrice(deliveryBuild.getPrice());
        setToName(deliveryBuild.getToName());
        break;
      case NORMAL:
        deliveryBuild = new DeliveryBuild(new NormalDelivery());
        setAdditionPrice(deliveryBuild.getPrice());
        setToName(deliveryBuild.getToName());
        break;
    }
  }
}
