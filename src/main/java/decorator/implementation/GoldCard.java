package decorator.implementation;

import decorator.BouquetDecoratorClass;

public class GoldCard extends BouquetDecoratorClass {

  private final double ADDITIONAL_PRICE = 50;
  private final String TO_NAME = " + Gold Card";

  public GoldCard() {
    setAdditionPrice(-ADDITIONAL_PRICE);
    setToName(TO_NAME);

  }

}
