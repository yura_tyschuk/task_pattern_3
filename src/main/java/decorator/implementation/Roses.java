package decorator.implementation;

import decorator.BouquetDecoratorClass;

public class Roses extends BouquetDecoratorClass {

  private final double ADDITIONAL_PRICE = 30;
  private final String TO_NAME = " + Rose";

  public Roses() {
    setAdditionPrice(ADDITIONAL_PRICE);
    setAdditionComponent(TO_NAME);
  }
}
