package decorator.implementation;

import decorator.BouquetDecoratorClass;

public class Tulips extends BouquetDecoratorClass {

  private final double ADDITIONAL_PRICE = 40;
  private final String TO_NAME = " + Tulips";

  public Tulips() {

    setAdditionPrice(ADDITIONAL_PRICE);
    setAdditionComponent(TO_NAME);
  }
}
