package decorator;

import flowers.implementation.BouquetDecorator;
import java.util.List;
import java.util.Optional;

public class BouquetDecoratorClass implements BouquetDecorator {

  private Optional<BouquetDecorator> bouquetOptional;
  private double additionalPrice;
  private String toName = "";
  private String additionComponent;

  public void setBouquet(BouquetDecorator outBouquet) {
    bouquetOptional = Optional.ofNullable(outBouquet);
    if (additionComponent != null) {
      bouquetOptional.orElseThrow(IllegalArgumentException::new).getFlowers()
          .add(additionComponent);
    }
  }

  protected void setAdditionPrice(double additionPrice) {
    this.additionalPrice = additionPrice;
  }

  protected void setAdditionComponent(String additionComponent) {
    this.additionComponent = additionComponent;
  }

  protected void setToName(String toName) {
    this.toName = toName;
  }


  @Override
  public double getCost() {
    return bouquetOptional.orElseThrow(IllegalArgumentException::new).getCost() + additionalPrice;
  }

  @Override
  public String getName() {
    return bouquetOptional.orElseThrow(IllegalArgumentException::new).getName() + toName;
  }

  @Override
  public List<String> getFlowers() {
    return bouquetOptional.orElseThrow(IllegalArgumentException::new).getFlowers();
  }
}
