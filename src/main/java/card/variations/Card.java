package card.variations;

public interface Card {

  String getToName();

  default int getPrice() {
    return 0;
  }
}
