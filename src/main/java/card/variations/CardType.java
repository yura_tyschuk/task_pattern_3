package card.variations;

public enum CardType {
  GOLD, SOCIAL, BONUS
}
