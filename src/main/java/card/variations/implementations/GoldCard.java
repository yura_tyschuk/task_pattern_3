package card.variations.implementations;

import decorator.BouquetDecoratorClass;
import decorator.implementation.Delivery;

public class GoldCard extends BouquetDecoratorClass {

  Delivery delivery = new Delivery();
  private final String TO_NAME = " + gold card";
  private final double ADDITIONAL_PRICE = delivery.getCost();

  public GoldCard() {
    setToName(TO_NAME);
    setAdditionPrice(ADDITIONAL_PRICE);
  }
}
