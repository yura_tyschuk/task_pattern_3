package card.variations.implementations;

public class SocialCard {

  private final String TO_NAME = " + social card";
  private double lastPrice;
  private final static int DISCOUNT = 20;


  public SocialCard(double lastPrice) {
    this.lastPrice = lastPrice;
  }

  public double getModifiedPrice() {
    return lastPrice - ((lastPrice * 20) / 100);
  }
}
