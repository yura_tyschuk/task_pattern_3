import decorator.BouquetDecoratorClass;
import events.Event;
import events.factory.implementation.ChooseEvent;
import events.factory.implementation.ForWhatBouquet;
import events.type.EventsType;
import factory.FlowersShop;
import factory.implementation.DniproShop;
import factory.implementation.KyivShop;
import factory.implementation.LvivShop;
import flowers.implementation.Bouquet;
import flowers.type.BouquetType;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Program {

  private static Logger logger = LogManager.getLogger();
  private static Scanner scanner = new Scanner(System.in);

  public static void main(String[] args) {

//
//    BouquetDecorator roses = new RosesBouquet();
//    BouquetDecoratorClass box = new Box(BoxType.TRACING);
//    BouquetDecoratorClass delivery = new Delivery(DeliveryType.FREE);
//    BouquetDecoratorClass discount = new GoldCard();
//    BouquetDecoratorClass rose = new Roses();
//    BouquetDecoratorClass tulips = new Tulips();
//
//    box.setBouquet(roses);
//    delivery.setBouquet(box);
//    discount.setBouquet(delivery);
//    rose.setBouquet(discount);
//    tulips.setBouquet(rose);
//
//    BouquetDecoratorClass fullBouquet = tulips;
//
//    double price = fullBouquet.getCost();
//    String name = fullBouquet.getName();
//    List<String> flowers = fullBouquet.getFlowers();
//
//    logger.warn("price: " + price);
//    logger.warn("name: " + name);
//    for (String s : flowers) {
//      logger.warn("flowers: " + s);
//    }
  }

  public FlowersShop chooseCity() {
    FlowersShop flowersShop = null;
    logger.warn("Choose your city: ");
    String city = scanner.nextLine();
    switch (city) {
      case "Lviv":
        flowersShop = new LvivShop();
        break;
      case "Kyiv":
        flowersShop = new KyivShop();
        break;
      case "Dnipro":
        flowersShop = new DniproShop();
        break;
    }
    return flowersShop;
  }

  public Bouquet chooseBouquet() {
    Bouquet bouquet = null;
    logger.warn("Choose what bouquet do you want: ");
    String bouquetString = scanner.nextLine();
    switch (bouquetString) {
      case "Rose":
        bouquet = chooseCity().prepare(BouquetType.ROSES);
        break;
      case "Tulips":
        bouquet = chooseCity().prepare(BouquetType.TULIPS);
        break;
      case "Peonies":
        bouquet = chooseCity().prepare(BouquetType.PEONIES);
        break;
    }
    return bouquet;
  }

  public Event chooseEvent() {
    ForWhatBouquet forWhatBouquet = new ChooseEvent();
    Event event = null;
    logger.warn("Choose for what event is your bouquet: ");
    String eventString = scanner.nextLine();
    switch (eventString) {
      case "Wedding":
        event = forWhatBouquet.prepare(EventsType.WEDDING);
        break;
      case "Birthday":
        event = forWhatBouquet.prepare(EventsType.BIRTHDAY);
        break;
      case "Party":
        event = forWhatBouquet.prepare(EventsType.PARTY);
        break;
    }
    return event;
  }

  public BouquetDecoratorClass choseDecorators() {


  }
}

