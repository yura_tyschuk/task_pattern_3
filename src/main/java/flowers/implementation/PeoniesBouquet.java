package flowers.implementation;

import java.util.ArrayList;
import java.util.List;

public class PeoniesBouquet implements Bouquet, BouquetDecorator {

  private static final double PRICE = 90;
  private String NAME = "Peonies bouquet ";
  private List<String> flowers;

  public PeoniesBouquet() {
    flowers = new ArrayList<>();
    flowers.add("5 peonies");
  }

  @Override
  public void prepare() {
    logger.warn("Bouquet prepared");
  }

  @Override
  public void sell() {
    logger.warn(" Peonies bouquet sold to customer");
  }

  @Override
  public double getCost() {
    return PRICE;
  }

  @Override
  public String getName() {
    return NAME;
  }

  @Override
  public List<String> getFlowers() {
    return flowers;
  }

}
