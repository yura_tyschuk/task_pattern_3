package flowers.implementation;

import java.util.List;

public interface BouquetDecorator {

  double getCost();

  String getName();

  List<String> getFlowers();

}
