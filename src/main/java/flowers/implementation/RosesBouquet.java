package flowers.implementation;

import java.util.ArrayList;
import java.util.List;

public class RosesBouquet implements Bouquet, BouquetDecorator {


  private static final double PRICE = 110;
  private String NAME = "Roses bouquet ";
  private List<String> flowers;

  public RosesBouquet() {
    flowers = new ArrayList<>();
    flowers.add("5 roses");
  }

  @Override
  public void prepare() {
    logger.warn("Roses bouquet prepared");
  }

  @Override
  public void sell() {
    logger.warn("Roses bouquet is sold to customer");
  }

  @Override
  public double getCost() {
    return PRICE;
  }

  @Override
  public String getName() {
    return NAME;
  }


  @Override
  public List<String> getFlowers() {
    return flowers;
  }
}
