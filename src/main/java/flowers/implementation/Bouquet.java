package flowers.implementation;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public interface Bouquet {

  Logger logger = LogManager.getLogger();

  void prepare();

  void sell();


}
