package flowers.implementation;

import java.util.ArrayList;
import java.util.List;

public class TulipsBouquet implements Bouquet, BouquetDecorator {


  private static final double PRICE = 100;
  private String NAME = "Tulips bouquet ";
  private List<String> flowers;

  public TulipsBouquet() {
    flowers = new ArrayList<>();
    flowers.add("4 tulips ");
  }

  @Override
  public void prepare() {
    logger.warn("Tulips bouquet is prepared");
  }

  @Override
  public void sell() {
    logger.warn("Tulips bouquet is sold to customer");
  }

  @Override
  public double getCost() {
    return PRICE;
  }

  @Override
  public String getName() {
    return NAME;
  }

  @Override
  public List<String> getFlowers() {
    return flowers;
  }

}
