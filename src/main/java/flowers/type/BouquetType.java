package flowers.type;

public enum BouquetType {
  TULIPS, ROSES, PEONIES
}
