package messages.logined.user;

public class ReadyOrderMessage {

  public String message;

  public ReadyOrderMessage() {
    message = " your order is ready";
  }

  public String getMessage() {
    return message;
  }

}
