package messages.logined.user;

import messages.MessageToLoginedUser;
import messages.MessagesToAllUsers;

public class Test {

  public static void main(String[] args) {
    MessagesToAllUsers messagesToAllUsers = new MessagesToAllUsers();
    messagesToAllUsers.sendMessagesToUsers();
    MessageToLoginedUser messageToLoginedUser = new MessageToLoginedUser();
    messageToLoginedUser.sendMessagesToLoginedUser();

  }
}
