package messages;

import messages.logined.user.BonusMessage;
import messages.logined.user.ReadyOrderMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import users.LoginUser;

public class MessageToLoginedUser {

  private static Logger logger = LogManager.getLogger();
  private LoginUser loginUser = new LoginUser();
  private String loginedUser;
  private BonusMessage bonusMessage = new BonusMessage();
  private ReadyOrderMessage readyOrderMessage = new ReadyOrderMessage();

  public MessageToLoginedUser() {
    this.loginedUser = loginUser.getLoginedUser();
  }

  public void sendMessagesToLoginedUser() {
    logger.warn(loginedUser + bonusMessage.getMessage());
    logger.warn(loginedUser + readyOrderMessage.getMessage());
  }


}
