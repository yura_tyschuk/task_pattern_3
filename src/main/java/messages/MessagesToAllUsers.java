package messages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import messages.all.users.NewProductsMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import users.ReadUsers;

public class MessagesToAllUsers {

  private static Logger logger = LogManager.getLogger();
  private HashMap<String, String> users;
  private NewProductsMessage newProductsMessage = new NewProductsMessage();

  public MessagesToAllUsers() {
    users = new HashMap<>();
  }

  private void getListUsers() {
    ReadUsers readUsers = new ReadUsers();
    readUsers.readUsersFromFile();

    users.putAll(readUsers.getUsersList());
  }

  public HashMap<String, String> getUsers() {
    return users;
  }

  public void sendMessagesToUsers() {
    getListUsers();
    for (int i = 0; i < users.size(); i++) {
      System.out.println(users.get(i) + newProductsMessage.getMessage());
    }
  }
}

