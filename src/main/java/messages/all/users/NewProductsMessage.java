package messages.all.users;

import java.util.ArrayList;
import java.util.List;

public class NewProductsMessage {

  private String message;
  private List<String> newProductList;


  public NewProductsMessage() {
    newProductList = new ArrayList<>();
    message = " we have new products! ";
  }

  public void setNewProducts(String newProduct) {
    this.newProductList.add(newProduct);
  }

  public String getMessage() {
    return message;
  }
}
