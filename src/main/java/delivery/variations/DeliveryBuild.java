package delivery.variations;

public class DeliveryBuild {

  Delivery delivery;

  public DeliveryBuild(Delivery delivery) {
    this.delivery = delivery;
  }

  public String getToName() {
    return delivery.getToName();
  }

  public int getPrice() {
    return delivery.getPrice();
  }
}
