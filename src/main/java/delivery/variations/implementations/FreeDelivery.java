package delivery.variations.implementations;

import delivery.variations.Delivery;

public class FreeDelivery implements Delivery {

  private final String TO_NAME = " + Free delivery";
  private final int ADDITIONAL_PRICE = 0;

  @Override
  public String getToName() {
    return TO_NAME;
  }

  @Override
  public int getPrice() {
    return ADDITIONAL_PRICE;
  }

}
