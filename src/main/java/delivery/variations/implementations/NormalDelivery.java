package delivery.variations.implementations;

import delivery.variations.Delivery;

public class NormalDelivery implements Delivery {

  private final String TO_NAME = " + Normal delivery";
  private final int ADDITIONAL_PRICE = 50;

  @Override
  public String getToName() {
    return TO_NAME;
  }

  @Override
  public int getPrice() {
    return ADDITIONAL_PRICE;
  }

}
