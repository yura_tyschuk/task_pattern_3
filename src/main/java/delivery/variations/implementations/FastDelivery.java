package delivery.variations.implementations;

import delivery.variations.Delivery;

public class FastDelivery implements Delivery {
  private final String TO_NAME = " + Fast delivery";
  private final int ADDITIONAL_PRICE = 100;

  @Override
  public String getToName() {
    return TO_NAME;
  }

  @Override
  public int getPrice() {
    return ADDITIONAL_PRICE;
  }

}
