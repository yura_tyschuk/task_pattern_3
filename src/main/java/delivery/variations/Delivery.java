package delivery.variations;

public interface Delivery {

  String getToName();

  int getPrice();

}
