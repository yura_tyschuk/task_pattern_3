package delivery.variations;

public enum DeliveryType {
  FAST, NORMAL, FREE
}
