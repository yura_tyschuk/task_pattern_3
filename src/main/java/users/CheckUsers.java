package users;

public class CheckUsers {

  private boolean checkUserExist;
  private String userToFind;
  private ReadUsers readUsers;

  public CheckUsers(String userToFind) {
    readUsers = new ReadUsers();
    readUsers.readUsersFromFile();
    this.userToFind = userToFind;
  }

  public boolean checkIfUserExist() {
    if (readUsers.getUsersList().containsKey(userToFind)) {
      checkUserExist = true;
    }
    return checkUserExist;
  }
}
