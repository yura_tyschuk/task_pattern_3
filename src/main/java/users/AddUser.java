package users;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class AddUser {

  private String user;
  private boolean checkUsers;
  private String FILE_PATH = "Users.txt";

  public void setUser(String user) {
    this.user = user + "\n";
  }

  public String getUser() {
    return user;
  }

  public void appendUsersToFile() {
    try {
      BufferedWriter out = new BufferedWriter(new FileWriter(FILE_PATH, true));
      out.write(getUser());
      out.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

}
