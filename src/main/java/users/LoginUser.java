package users;

import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LoginUser {

  private String loginedUser;
  private CheckUsers checkUsers;
  private static Logger logger = LogManager.getLogger();
  private Scanner scanner = new Scanner(System.in);
  private AddUser addUser;

  public LoginUser() {
    addUser = new AddUser();
  }

  public void enter() {

    logger.warn("Enter your username: ");
    loginedUser = scanner.nextLine();
    checkUsers = new CheckUsers(loginedUser);
    if (checkUsers.checkIfUserExist()) {
      logger.warn("You successfully logined");
    } else {
      logger.warn("Do you want to create new user?");
      String confirm = scanner.nextLine();

      switch (confirm) {
        case "Y":
          logger.warn("Print user login: ");
          loginedUser = scanner.nextLine();
          addUser.setUser(loginedUser);
          addUser.appendUsersToFile();
          break;

        case "N":
          break;
      }
    }
  }

  public String getLoginedUser() {
    return loginedUser;
  }
}




