package users;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class ReadUsers {

  private final String FILE_PATH = "Users.txt";
  HashMap<String, String> usersMap;

  public ReadUsers() {
    usersMap = new HashMap<>();

  }

  public void readUsersFromFile() {
    try {

      String line;
      BufferedReader reader = new BufferedReader(new FileReader(FILE_PATH));
      while ((line = reader.readLine()) != null) {
        String[] parts = line.split(":", 2);
        if (parts.length >= 2) {
          String key = parts[0];
          String value = parts[1];
          usersMap.put(key, value);
        } else {
          System.out.println("ignoring line: " + line);
        }
      }

      for (String key : usersMap.keySet()) {
        System.out.println(key + ":" + usersMap.get(key));
      }
      reader.close();

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public HashMap<String, String> getUsersList() {
    return usersMap;
  }

}
