package box.variations.implementations;

import box.variations.Box;

public class TracingVariations implements Box {
  private final String TO_NAME = " + Tracing";
  private final int ADDITIONAL_PRICE =  25;


  @Override
  public String getToName() {
    return TO_NAME;
  }

  @Override
  public int getPrice() {
    return ADDITIONAL_PRICE;
  }
}
