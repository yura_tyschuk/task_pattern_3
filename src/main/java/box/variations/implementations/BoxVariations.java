package box.variations.implementations;

import box.variations.Box;

public class BoxVariations implements Box {
  private final String TO_NAME = " + Box";
  private final int ADDITIONAL_PRICE =  50;


  @Override
  public String getToName() {
    return TO_NAME;
  }

  @Override
  public int getPrice() {
    return ADDITIONAL_PRICE;
  }
}
