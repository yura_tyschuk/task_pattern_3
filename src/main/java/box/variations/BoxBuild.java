package box.variations;

public class BoxBuild {
  Box box;

  public BoxBuild(Box box) {
    this.box = box;
  }

  public String getToName() {
    return box.getToName();
  }

  public int getPrice() {
    return box.getPrice();
  }
}
