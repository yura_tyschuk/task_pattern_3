package box.variations;

public interface Box {

  String getToName();

  int getPrice();

}
